﻿namespace TasksChaining
{
    public static class CommonTasks
    {
        /// <summary>
        /// Returns an array of 10 random integers.
        /// </summary>
        /// <returns>Array of random integers</returns>
        public static int[] CreateArray()
        {
            var r = new Random();
            var result = Enumerable.Range(0, 10).Select(x => r.Next());
            Console.WriteLine($"Created array: ({string.Join(",", result.Select(x => x.ToString()))})");

            return result.ToArray();
        }

        /// <summary>
        /// Returns an array multiplied by a number.
        /// </summary>
        /// <param name="array">array to multiply</param>
        /// /// <param name="multiplier">multiplier</param>
        /// <returns>Multiplied array</returns>
        public static int[] MultiplyArray(int[] array, int multiplier)
        {
            var result = array.Select(x => x * multiplier);
            Console.WriteLine($"Multiplied array: ({string.Join(",", result.Select(x => x.ToString()))})");
 
            return result.ToArray();
        }

        /// <summary>
        /// Returns a sorted array.
        /// </summary>
        /// <param name="array">array to sort</param>
        /// <returns>Sorted array</returns>
        public static int[] SortArray(int[] array)
        {
            var result = array.OrderBy(x => x).ToArray();
            Console.WriteLine($"Sorted array: ({string.Join(",", result.Select(x => x.ToString()))})");

            return result;
        }

        /// <summary>
        /// Returns the average value of the array.
        /// </summary>
        /// <param name="array">array to calculate average</param>
        /// <returns>Average value of the array</returns>
        public static double CalculateArrayAverage(int[] array)
        {
            if (array.Length == 0)
            {
                return 0.0d;
            }

            var result = array.Average();
            Console.WriteLine($"Average of the array: {result}");

            return result;
        }

        /// <summary>
        /// Returns the average value calculated by chained tasks.
        /// </summary>
        /// <returns>Average value calculated by chained tasks.</returns>
        public static async Task<double> Main()
        {
            var task = Task.Run(
                () =>
                {
                    return CreateArray();
                });

            Task<int[]> continuation1 = task.ContinueWith(
                    antecedent =>
                    {
                        return MultiplyArray(antecedent.Result, 2);
                    }, TaskContinuationOptions.OnlyOnRanToCompletion);

            Task<int[]> continuation2 = continuation1.ContinueWith(
                    antecedent =>
                    {
                        return SortArray(antecedent.Result);
                    }, TaskContinuationOptions.OnlyOnRanToCompletion);

            Task<double> continuation3 = continuation2.ContinueWith(
                    antecedent =>
                    {
                        return CalculateArrayAverage(antecedent.Result);
                    }, TaskContinuationOptions.OnlyOnRanToCompletion);
            
            double result = await continuation3;

            return result;
        }
    }
}
