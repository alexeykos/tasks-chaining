﻿namespace TasksChaining.Tests
{
    public class CommonTests
    {
        [Test]
        public void CreateArray_ReturnsArrayWithTenElements()
        {
            // Act
            int[] result = CommonTasks.CreateArray();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Has.Length.EqualTo(10));
            Assert.That(result, Is.InstanceOf(typeof(Array)));
        }

        [TestCase(new int[0], 5, ExpectedResult = new int[0])]
        [TestCase(new[] { 4, 1, 9, 33, 25, 100, 8, -4, -44 }, 2, ExpectedResult = new[] { 8, 2, 18, 66, 50, 200, 16, -8, -88 })]
        [TestCase(new[] { 2, 14, 5, 33, 25, 200, 27, 0, -3, -1 }, 3, ExpectedResult = new[] { 6, 42, 15, 99, 75, 600, 81, 0, -9, -3 })]
        [TestCase(new[] { -1, 13, 3, 21, 49, 27, 0, 0, 1000 }, -5, ExpectedResult = new[] { 5, -65, -15, -105, -245, -135, 0, 0, -5000 })]
        public int[] MultiplyArray_Tests(int[] array, int multiplier)
        {
            // Act
            return CommonTasks.MultiplyArray(array, multiplier);
        }

        [TestCase(new int[0], ExpectedResult = new int[0])]
        [TestCase(new[] { 4, 1, 9, 33, 25, 100, 8, 0, -7, 1 }, ExpectedResult = new[] {-7, 0, 1, 1, 4, 8, 9, 25, 33, 100 })]
        [TestCase(new[] { 2, 14, 5, 33, 25, 200, 27, -40, -5, 5 }, ExpectedResult = new[] { -40, -5, 2, 5, 5, 14, 25, 27, 33, 200 })]
        public int[] SortArray_Tests(int[] array)
        {
            // Act
            return CommonTasks.SortArray(array);
        }

        [TestCase(new int[0], ExpectedResult = 0.0d)]
        [TestCase(new[] { 4, 1, 9, 33, 25, 100, 8, -4, -55, -1 }, ExpectedResult = 12.0d)]
        [TestCase(new[] { 2, 14, 5, 33, 25, 200, 27, 0, -1000, 4000 }, ExpectedResult = 330.60000000000002d)]
        public double CalculateArrayAverage_Tests(int[] array)
        {
            // Act
            return CommonTasks.CalculateArrayAverage(array);
        }
    }
}
