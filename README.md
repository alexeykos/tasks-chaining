## Task Chaining and Continuation ##


To complete the task, you need to write a program that executes a chain of four tasks:

Task 1: creates an array of 10 random integers.
Task 2: multiplies the array by a randomly generated number.
Task 3: sorts the array by ascending.
Task 4: calculates the average value.

Apply separate methods to implement required actions and convert the methods to corresponding tasks. Don't forget to cover all methods with unit test.

All tasks should print the values to the console. Use TPL continuations in your implementation.

To successfully complete the task, review the following chapters from the guide Chaining Tasks Using Continuation Tasks:

[Create a Continuation for a Single Antecedent](https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/chaining-tasks-by-using-continuation-tasks#create-a-continuation-for-a-single-antecedent)

[Pass Data to a Continuation](https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/chaining-tasks-by-using-continuation-tasks#pass-data-to-a-continuation)


Additionally, check the article on the Random class which might come in handy while creating an array for random integers.
